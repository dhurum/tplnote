#!/usr/bin/python

from PyQt5 import QtWidgets
import sys
from pathlib import Path
from library.subject_parser import SubjectParser, ParserException
from library.widgets import MainWindow

def getSubject(path):
    with open(path.as_posix(), "r") as f:
        return SubjectParser().parse(f)

app = QtWidgets.QApplication(sys.argv)

path = Path("subjects")
subject_files = list(path.glob("*.tpl"))

subjects = []

try:
    for subject_file in subject_files:
        subjects.append(getSubject(subject_file))
except ParserException as e:
    error_text = "In file %s in line %d, position %d:\n\n%s%s^\n%s" % (
            subject_file, e.line, e.position, e.text,
            "".join(" " for i in range(e.position - 1)), e)
    print(error_text)
    QtWidgets.QMessageBox.critical(QtWidgets.QWidget(),
            "Error in template %s" % subject_file, "<pre>%s</pre>" % error_text)
    sys.exit()

window = MainWindow(subjects)
window.show()

sys.exit(app.exec_())
