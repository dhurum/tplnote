import re

class ParserException(Exception):
    def __init__(self, error):
        self.error = error
        self.position = None
        self.line = None
        self.text = None

    def __str__(self):
        return self.error

    def setPosition(self, position):
        self.position = position

    def setLine(self, line):
        self.line = line

    def setText(self, text):
        self.text = text

class LexerToken:
    def __init__(self, token, group_id):
        self.group_id = group_id
        self.groups_num = re.compile(token[0]).groups
        self.action = token[1]

        if len(token) > 2:
            self.state = token[2]
        else:
            self.state = None

    def process(self, match):
        if not self.action:
            return

        if self.groups_num == 0:
            self.action(match.group())
        else:
            self.action(match.groups()[self.group_id : self.group_id
                + self.groups_num + 1])

class Lexer:
    def __init__(self, tokens):
        self.tokens = {}
        self.regex = {}

        if "root" not in tokens:
            raise Exception("No 'root' state in tokens")

        for state in tokens:
            tokens_num = len(tokens[state])
            group_id = 0
            regex_groups = []
            self.tokens[state] = {}

            def addToken(i, token):
                key = "_%d" % i
                nonlocal group_id
                self.tokens[state][key] = LexerToken(token, group_id)
                group_id += 1 + self.tokens[state][key].groups_num
                regex_groups.append("(?P<%s>%s)" % (key, token[0]))

            for i in range(tokens_num):
                addToken(i, tokens[state][i])

            addToken(tokens_num, (r".", self.error))
            self.regex[state] = re.compile("|".join(regex_groups))

    def error(self, text):
        raise ParserException("Unexpected symbol '%s'" % text)

    def process(self, text):
        state = "root"
        state_stack = []
        regex_iter = self.regex[state].finditer(text)

        while True:
            try:
                match = next(regex_iter)
            except StopIteration:
                break

            token = self.tokens[state][match.lastgroup]
            try:
                token.process(match)
            except ParserException as e:
                e.setText(text)
                e.setPosition(match.start() + 1)
                raise

            if token.state:
                if token.state == "#pop":
                    if len(state_stack) == 0:
                        raise Exception("Can't pop empty state stack")
                    state = state_stack.pop()
                elif token.state == "#push":
                    state_stack.append(state)
                else:
                    state_stack.append(state)
                    state = token.state

                regex_iter = self.regex[state].finditer(text, match.end())

class Parser:
    def __init__(self):
        self.lexer = Lexer(self.tokens)

    def run(self, f):
        line_id = 1

        for line in f:
            try:
                self.lexer.process(line)
            except ParserException as e:
                e.setLine(line_id)
                raise

            line_id += 1
