from PyQt5 import QtWidgets, QtGui, QtCore
import os

class GrowingTextEdit(QtWidgets.QTextEdit):
    def __init__(self, *args):
        QtWidgets.QTextEdit.__init__(self, *args)  
        self.document().contentsChanged.connect(self.sizeChange)

        test_doc = QtGui.QTextDocument()
        test_doc.setDefaultFont(self.currentFont())
        test_doc.setPlainText("A");
        height = test_doc.size().height() + 4

        self.setFixedHeight(height)
        self.setTabChangesFocus(True)

    def sizeChange(self):
        height = self.document().size().height() + 4
        self.setFixedHeight(height)

    def getValue(self):
        return self.document().toPlainText()

    def isDirty(self):
        return self.document().toPlainText() != ""

    def clear(self):
        self.document().setPlainText("")

class Combobox(QtWidgets.QComboBox):
    def __init__(self, values):
        QtWidgets.QComboBox.__init__(self)
        self.setEditable(True)

        self.values = values

        for value in values:
            self.addItem(value)

    def getValue(self):
        return self.currentText()

    def isDirty(self):
        return self.currentText() != self.values[0]

    def clear(self):
        self.setCurrentIndex(0)

class Field:
    def destructGui(self):
        self.field = None

    def getValue(self):
        if not self.field:
            return None
        return self.field.getValue()

    def focus(self):
        if self.field:
            self.field.setFocus()

    def isDirty(self):
        if not self.field:
            return False

        return self.field.isDirty()

    def clear(self):
        if self.field:
            self.field.clear()

class TextField(Field):
    def __init__(self, name):
        self.name = name
        self.field = None

    def addToGui(self, layout):
        if not self.field:
            self.field = GrowingTextEdit()

        layout.addRow(self.name, self.field)

class ComboboxField(Field):
    def __init__(self, name, values):
        self.name = name
        self.field = None
        self.values = values

    def addToGui(self, layout):
        if not self.field:
            self.field = Combobox(self.values)

        layout.addRow(self.name, self.field)

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, subjects):
        QtWidgets.QMainWindow.__init__(self)

        self.fields = []
        self.subject = None
        
        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_area.setWidgetResizable(True)

        self.scroll_content = QtWidgets.QWidget()
        self.scroll_area.setWidget(self.scroll_content)

        self.setCentralWidget(self.scroll_area)

        self.subjects_menu = self.menuBar().addMenu("Subject")

        for subject in subjects:
            entry = self.subjects_menu.addAction(subject.name)
            entry.triggered.connect(
                    lambda x, subject=subject: self.showSubjectEditor(subject))

        self.note_toolbar = self.addToolBar("Note")
        
        self.clear_action = self.note_toolbar.addAction("Clear")
        self.clear_action.triggered.connect(self.tryClear)
        self.clear_action.setShortcut(QtCore.Qt.Key_N | QtCore.Qt.CTRL);
        
        self.save_action = self.note_toolbar.addAction("Save")
        self.save_action.triggered.connect(self.save)
        self.save_action.setShortcut(QtCore.Qt.Key_S | QtCore.Qt.CTRL);

        self.layout = QtWidgets.QFormLayout(self.scroll_content)
        self.showSubjectEditor(subjects[0])

    def closeEvent(self, ev):
        if not self.checkUnsavedChanges():
            ev.ignore()

    def checkUnsavedChanges(self):
        if self.subject.isDirty():
            ret = QtWidgets.QMessageBox.warning(self, "Pending changes",
                    "The note has been modified.\nDo you want to save your changes?",
                    QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Discard
                    | QtWidgets.QMessageBox.Cancel)

            if ret == QtWidgets.QMessageBox.Save:
                return self.save()
            elif ret == QtWidgets.QMessageBox.Cancel:
                return False

        return True

    def removeFields(self):
        while self.layout.count() > 0:
            item = self.layout.takeAt(0)
            if not item:
                continue
       
            widget = item.widget()
            if widget:
                widget.deleteLater()

        self.subject.destructGui()


    def showSubjectEditor(self, subject):
        if self.subject:
            if not self.checkUnsavedChanges():
                return
            self.removeFields()

        self.subject = subject;
        self.setWindowTitle("Notes - " + subject.name)

        subject.constructGui(self.layout) 

    def tryClear(self):
        if not self.checkUnsavedChanges():
            return

        self.subject.clearGui()

    def save(self):
        if not self.subject.isDirty():
            return True

        (filename, f) = QtWidgets.QFileDialog.getSaveFileName(self, "Save",
                self.subject.save_dir)

        if not len(filename):
            return False

        with open(filename, "w") as f:
            for text in self.subject.getFilledTemplate():
                f.write(text)

        self.subject.clearGui()
        return True
