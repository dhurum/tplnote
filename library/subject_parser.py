from library.subject import Subject, SubjectException
from library.parser import *

class SubjectParser(Parser):
    def __init__(self):
        self.tokens = {
                "root": [
                    (r"{#(\w+): *([^#]+)#}.*\n", self.parseCfgValue),
                    (r"{%([^%{]*)({([^%}]+)})?%}", self.parseFieldValue),
                    (r"{\$([^${]*)({([^$}]+)})?\$}", self.parseFieldName),
                    (r"[^{]+", self.parseText)
                    ]
                }
        Parser.__init__(self)

    def parseText(self, token):
        self.subject.addPlain(token)

    def parseCfgValue(self, tokens):
        if tokens[1] == "save_directory":
            self.subject.setSaveDir(tokens[2])
        elif tokens[1] == "name":
            self.subject.setName(tokens[2])

    def parseFieldValue(self, tokens):
        values = tokens[3]
        if values:
            values = values.split("|")

            if len(values) < 2:
                raise ParserException( "You need to specify at least 2 values")

        try:
            self.subject.addFieldValue(tokens[1], values)
        except SubjectException as e:
            raise ParserException(str(e))

    def parseFieldName(self, tokens):
        values = tokens[3]
        if values:
            values = values.split("|")

            if len(values) < 2:
                raise ParserException( "You need to specify at least 2 values")

        self.subject.addFieldName(tokens[1], values)

    def parse(self, f):
        self.subject = Subject()
        self.run(f)
        return self.subject
