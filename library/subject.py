from library.widgets import TextField, ComboboxField
import os

class PlainToken:
    def __init__(self, text):
        self.text = text

    def getText(self):
        return self.text

class ValueToken:
    def __init__(self, field, subject):
        self.field = field
        self.subject = subject

    def getText(self):
        return self.subject.getFieldValue(self.field)

class SubjectException(Exception):
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return self.text

class Subject:
    def __init__(self):
        self.tokens = []
        #list for nice order
        self.fields = []
        self.fields_map = {}
        self.last_field = None
        self.save_dir = "."
        self.name = "Unnamed"

    def setSaveDir(self, save_dir):
        if save_dir == "":
            save_dir = "."

        self.save_dir = os.path.abspath(os.path.expanduser(save_dir))

    def setName(self, name):
        self.name = name

    def addPlain(self, text):
        self.tokens.append(PlainToken(text))

    def addField(self, field, values):
        if field not in self.fields_map:
            if values:
                self.fields_map[field] = ComboboxField(field, values)
            else:
                self.fields_map[field] = TextField(field)
            self.fields.append(field)

    def addFieldName(self, field, values):
        field = field.strip()

        self.addField(field, values)

        self.tokens.append(PlainToken(field))
        self.last_field = field

    def addFieldValue(self, field, values):
        field = field.strip()

        if field == "":
            if not self.last_field:
                raise SubjectException(
                        "Using field value without declaring field first")

            field = self.last_field
        
        self.addField(field, values)

        self.tokens.append(ValueToken(field, self))
        self.last_field = None

    def getFieldValue(self, field):
        return self.fields_map[field].getValue()

    def getFilledTemplate(self):
        for token in self.tokens:
            yield token.getText()

    def constructGui(self, layout):
        for field in self.fields:
            self.fields_map[field].addToGui(layout)

        if len(self.fields):
            self.fields_map[self.fields[0]].focus()

    def destructGui(self):
        for field in self.fields_map:
            self.fields_map[field].destructGui()

    def isDirty(self):
        for field in self.fields_map:
            if self.fields_map[field].isDirty():
                return True

        return False

    def clearGui(self):
        for field in self.fields_map:
            self.fields_map[field].clear()

        if len(self.fields):
            self.fields_map[self.fields[0]].focus()

