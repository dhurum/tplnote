## Small application for creating text files based on templates

Tplnote offers a simple gui for creating text files based on templates.  
To use it, create some templates, put them into `subjects` folder, and run `tplnote.py`

### Templates syntax:
* `{#...#}` - Configuration value;
* `{$...$}` - Input field name;
* `{%...%}` - Input field value;

#### Configuration:
* `{#name: ...#}` - Subject name;
* `{#save_directory: ...#}` - Save directory for text files, by default current one;

#### Fields:
**Input field name** means that corresponding input will be added to gui, and the name would be inserted in resulting text.  
**Input field value** means that corresponding input will be added to gui (if this is not an empty tag, see below), and it's value would be used in resulting text.  

* `{$name$}` - Create field *name* in gui, and insert *name* into text;
* `{%%}` - Insert value of last field name's input (can be used once for each field name);
* `{%name%}` - Create field *name* in gui, and insert it's value into text;

By default, fields are text inputs. You can also create a combobox, using the following syntax: `Name {value 1|value 2|value 3}`

### Example:
```
{#name: Biology#}
{#save_directory: ~/#}
{%Latin Name%} ({%Common Name%})

This is {%Type {aqatic|terrestrial}%} animal, with {%Horns Number%} horns.

{$Description$}: {%%}
{$Feeding type {predator|herbivore}$}: {%%}
```

In this example subject's name is *Biology*, default save folder is home, and it creates a number of input fields.  
*Latin Name*, *Common Name*, *Horns Number* and *Description* are text inputs, *Type* and *Feeding type* are comboboxes.  
In resulting text would be used values of *Latin Name*, *Common Name*, *Type* and *Horns Number*, and both names and values of *Description* and *Feeding type*.
